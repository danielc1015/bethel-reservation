<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::resource('allocation', 'AllocationController');
Route::resource('user', 'UserController');
Route::get('reservation/user/{user_id}', 'ReservationController@indexByUser');
Route::get('reservation/allocation/{allocation_id}', 'ReservationController@index');
Route::get('reservation/allocation/{allocation_id}/user/{user_id}', 'ReservationController@index');
Route::get('reservation/export/{from}/{to}', 'ReservationController@export');
Route::delete('reservation/{id}', 'ReservationController@destroy');
Route::post('/reservation', 'ReservationController@store');
Route::get('availability/{date}/allocation/{allocation_id}', 'AvailabilityController@indexByDateAndAllocation');
Route::resource('availability', 'AvailabilityController');
Route::post('auth/signin', 'AuthController@signIn');