<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class AllocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('allocations')->insert([
            'name' => 'Gimnasio Varones',
            'maxPeopleSameTime' => 2,
            'blockDuration' => 15,
            'maxBlocksAllowed' => 3,
            'start_time' => "05:00:00",
            'end_time' => "22:00:00",
        ]);
        \DB::table('allocations')->insert([
            'name' => 'Gimnasio Damas',
            'maxPeopleSameTime' => 2,
            'blockDuration' => 15,
            'maxBlocksAllowed' => 3,
            'start_time' => "06:00:00",
            'end_time' => "22:00:00",
        ]);
        \DB::table('allocations')->insert([
            'name' => 'Multicancha',
            'maxPeopleSameTime' => 1,
            'blockDuration' => 30,
            'maxBlocksAllowed' => 3,
            'start_time' => "05:00:00",
            'end_time' => "22:00:00",
        ]);
    }
}
