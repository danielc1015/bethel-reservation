<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        \DB::table('users')->truncate();
        \DB::table('users')->insert([
            [
                "id" => "5933129",
                "name" => "Negrete Mauna, Victor Antonio",
                "password" => "fefs3",
                "role_id" => 2
            ],
            [
                "id" => "5931149",
                "name" => "Faúndez Casanova, Benjamín Gabriel", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5924689",
                "name" => "Maldonado, Paula Magdalena","password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5925047",
                "name" => "Córdova Leiva, Daniel Antonio", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5925048",
                "name" => "Córdova, Lilette Adelaida", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5930661",
                "name" => "Villalobos Angel, César Nicolás", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5924332",
                "name" => "Romero Rojas, Dámaris Natalia", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5929448",
                "name" => "Villalobos Ángel, Franco Antonio", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5934378",
                "name" => "Guzmán Bruna, Esteban Alejandro", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5931927",
                "name" => "Inostroza Sáez, Amanda Antonia", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5928051",
                "name" => "Basualdo Iturralde, Jimena Edith", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5935805",
                "name" => "Toro Menares, Simón Nicolás Natanael", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5938991",
                "name" => "Espinoza Sandoval, Vicente Ignacio", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5937388",
                "name" => "García Encina, Gonzalo Andrés", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5937873",
                "name" => "Mateu Sánchez, Josep Andreu", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5924690",
                "name" => "Maldonado Peña, Miguel Ángel", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5924212",
                "name" => "Vidal Melivilú, Katherine Alejandra", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "859976",
                "name" => "Kim, Kay S.", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "859975",
                "name" => "Kim, Charlie", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "51241",
                "name" => "Smith, Dorothea M. J.", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5912733",
                "name" => "Morrás, Marianne", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "842753",
                "name" => "Reed, Jason Daniel", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5913659",
                "name" => "González Valenzuela, Jorge Cinecio", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5911389",
                "name" => "Lovato, Elsa Rosa", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5912687",
                "name" => "Lovato Grosso, Pedro Juan", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5915433",
                "name" => "González, Stacie", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5912725",
                "name" => "Morrás Oyanedel, Fernando Gonzalo", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5925064",
                "name" => "Gallegos Vásquez, Claudio Rafael", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5925063",
                "name" => "Gallegos, Evelin Nidia", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5924963",
                "name" => "Roldán, Natalia Macarena", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5924962",
                "name" => "Roldán Ledezma, Cristian Allan", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5924795",
                "name" => "Calderón Adaos, Matías Francisco", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5915450",
                "name" => "Cofré Marambio, Arturo", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5925072",
                "name" => "Guerrero Sánchez, Kevin Maximiliano", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5924650",
                "name" => "Fuentes Abstanger, Diego Andrés", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5915446",
                "name" => "Rodríguez, Vanessa Genoveva", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5924143",
                "name" => "Bahamondes, Jade", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5924115",
                "name" => "Bahamondes Gutiérrez, Juan Pablo", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5924647",
                "name" => "Koch, Amparo", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5914337",
                "name" => "Tabilo, María Paz", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5925079",
                "name" => "Benavides Huenchulao, Jorge Diego", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5928865",
                "name" => "Cerda Acevedo, David", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5930268",
                "name" => "Parada Meza, Matías Bernabé Lucas", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5924646",
                "name" => "Koch Vignola, Ernesto Michael", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5912784",
                "name" => "Mercado Pérez, Rodolfo Eliseo", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5934724",
                "name" => "Díaz Olivares, Jonathan Florentino", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5937563",
                "name" => "Martínez Gómez, Pablo Rodrigo", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5935440",
                "name" => "Gutiérrez Saldías, Daniel Arturo", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5935801",
                "name" => "Guerra Guzmán, Nelson Nicolás", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5912776",
                "name" => "Tabilo Caniuqueo, Moisés Segundo", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5912709",
                "name" => "Silva Vásquez, Isabel", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5935674",
                "name" => "Tardone González, Ignacio Alejandro", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5910064",
                "name" => "Aro, Rosario Emperatriz", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5910056",
                "name" => "Aro Rojas, Florencio Segundo", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5925102",
                "name" => "Castro Velásquez, Daniel Alfredo", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5913438",
                "name" => "Rodríguez Sánchez, Braulio Anselmo", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5924633",
                "name" => "Tucki Castro, Alejandro Daniel", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5924525",
                "name" => "Díaz Ojeda, Diego Francisco", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5924608",
                "name" => "Arriagada Pavez, Luis Patricio", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5924609",
                "name" => "Tabilo Torres, Danko Estefano", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5923994",
                "name" => "Cofré, Valeria Isabel", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5924241",
                "name" => "Arriagada, Katherine Constanza", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5924229",
                "name" => "Godoy Brash, Elizabeth Roxana", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5916999",
                "name" => "Donoso Pavez, María José", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5924060",
                "name" => "Figueroa Chávez, Ricardo Octavio", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5923476",
                "name" => "González, Gabriela Isabel", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5924022",
                "name" => "Ovalle Fernández, José Tomás", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5918469",
                "name" => "Rojas, Priscila Andrea", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5922747",
                "name" => "Lobo Hernández, Michel Alexander", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5923900",
                "name" => "Kim Kim, Allen Chris", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5923860",
                "name" => "García, Christel Desiree", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5917864",
                "name" => "Isasmendi, Maribel", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5923843",
                "name" => "Isasmendi Cartagena, Ernesto Manuel", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5923779",
                "name" => "Rojas Baeza, Jorge Luis", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5918874",
                "name" => "Pereiro, Ximena Susana", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5923371",
                "name" => "Astudillo, Johanna Angélica", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5924269",
                "name" => "Cerda Paiva, Juan Pablo", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5924270",
                "name" => "Cerda, Carla Andrea", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5924256",
                "name" => "Uribe Rojas, Samuel Andrés", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5924318",
                "name" => "Basáez, Lorena", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5924574",
                "name" => "Fuentes Peña, Ricardo Alexander", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5924550",
                "name" => "Pereira, Karina", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5915015",
                "name" => "González Venegas, Yuri Pablo", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5641851",
                "name" => "Pereiro, Juan Manuel", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5924529",
                "name" => "Peralta Leiva, Pablo Isaías", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5924161",
                "name" => "Figueroa, Damaris Jasmin", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5924438",
                "name" => "Ávila Vega, Lucas Cristobal", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5924317",
                "name" => "Basáez Durán, Rodrigo Andrés", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5915244",
                "name" => "Puentes Muñoz, Mauricio Gonzalo", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5915279",
                "name" => "García Mercado, José Gonzalo", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5924549",
                "name" => "Pereira Rossi, Carlos Patricio", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5924321",
                "name" => "Ogalde Vega, Ivo Isaac", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5924385",
                "name" => "Martínez Molina, Mario Isaac", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5915378",
                "name" => "Astudillo Angulo, Samuel Antonio", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5924322",
                "name" => "Ogalde, Carolina Angélica", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5929708",
                "name" => "Sánchez Leñero, Cristóbal Andrés", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5938007",
                "name" => "Candia Pereira, Daniel", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5937503",
                "name" => "Karmy Espinosa, Ignacio Andrés", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5931131",
                "name" => "López Jaque, David Esteban", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5938707",
                "name" => "Larsen Sønderskov, Patrick Jonas", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5924639",
                "name" => "Ortiz Flores, Adrian Nicolás", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5930267",
                "name" => "Valdebenito Vasquez, Sebastián", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5924474",
                "name" => "Altamirano Vega, Francisco Raúl", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5924659",
                "name" => "Flores Ramírez, Cristóbal Martín", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5924046",
                "name" => "Sandoval, Constanza Paola", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5923893",
                "name" => "Sandoval Aliaga, Óliver Eduardo", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5927618",
                "name" => "Tapia Luque, Yerko", "password" => "fefs3", "role_id" => 2
            ],
            [
                "id" => "5924577",
                "name" => "Navarro Véliz, Hugo Esteban", "password" => "fefs3", "role_id" => 2
            ]
        ]);
        Schema::enableForeignKeyConstraints();
    }
}
