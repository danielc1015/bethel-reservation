<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Reservation\IReservationRepository;
use App\Reservation\AdminReservationRepository;
use App\Reservation\UserReservationRepository;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        app()->bind(IReservationRepository::class, function () {
            return function ($user) {
                if ($user != null && $user->role()->name == 'Admin') {
                    return new AdminReservationRepository();
                } else {
                    return new UserReservationRepository();
                }
            };
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
