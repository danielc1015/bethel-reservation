<?php

namespace App\Allocation;

use App\Allocation\Allocation;

class AllocationRepository
{
    public function findAll()
    {
        return Allocation::all();
    }

    public function findById($allocation_id)
    {
        return Allocation::findOrFail($allocation_id);
    }

    public function create($allocation)
    {
        # code...
    }

    public function edit($allocation_id, $allocation)
    {
        # code...
    }
}
