<?php

namespace App\Allocation;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Allocation extends Model
{
    use HasFactory;

    public function reservations()
    {
        return $this->hasMany('App\Models\Reservation');
    }

    public function availabilities()
    {
        return $this->hasMany('App\Models\Availability');
    }
}
