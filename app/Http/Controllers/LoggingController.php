<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LoggingController extends Controller
{
    public function index()
    {
        $path = storage_path('logs/laravel.log');
        return response()->file($path);
    }
}
