<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User\UserRepository;

class AuthController extends Controller
{
    private $userRepository;

    public function __construct(UserRepository $userRepository) {
        $this->userRepository = $userRepository;
    }
    
    public function signIn(Request $request)
    {
        $id =       $request->input('user_id');
        $password = $request->input('password');

        $user = $this->userRepository->findByIdAndPassword($id, $password);
        return response()->json($user, 200);
    }
}
