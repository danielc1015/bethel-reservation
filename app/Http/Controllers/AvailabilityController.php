<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Availability\Availability;
use App\Models\Allocation;
use Illuminate\Support\Carbon;

use App\Availability\AvailabilityDateAndTimeFormater;
use App\Availability\AvailabilityRepository;
use App\Allocation\AllocationRepository;

class AvailabilityController extends Controller
{
    private $allocationRepository;
    private $availabilityRepository;

    public function __construct(AvailabilityRepository $availabilityRepository, AllocationRepository $allocationRepository) {
        $this->availabilityRepository = $availabilityRepository;
        $this->allocationRepository = $allocationRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $today = now()->format('Y-m-d');
        $availability = Availability::where('date', '>=', $today)->get();

        return response()->json($availability, 200);

    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexByDateAndAllocation($date, $allocation_id)
    {
        $availability = $this->availabilityRepository->findByDateAndAllocation($date, $allocation_id);

        return response()->json($availability, 200);

    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $availabilityInput = $request->input('availability');
        $allocation = $this->allocationRepository->findById($availabilityInput['allocation_id']);

        $availabilityDateAndTimeFormater = new AvailabilityDateAndTimeFormater();
        $blocks = $availabilityDateAndTimeFormater($availabilityInput['start_date'], $availabilityInput['end_date'], $allocation->start_time, $allocation->end_time, $allocation->blockDuration);
        $availabilityRecords = $this->availabilityRepository->create($allocation->id, $availabilityInput['availability'], $blocks);

        return response()->json('created', 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
