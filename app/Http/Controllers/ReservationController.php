<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reservation\IReservationRepository;
use App\Availability\AvailabilityRepository;
use App\Reservation\MakeReservation;
use App\Reservation\DeleteReservation;
use App\Exports\ReservationsExport;
use Maatwebsite\Excel\Facades\Excel;

class ReservationController extends Controller
{

    private $repo;

    public function __construct() {
        $this->repo = app(IReservationRepository::class)(auth()->user());
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($allocation_id, $user_id = null)
    {

        $reservations = $user_id == null ? $this->repo->findAll($allocation_id) : $this->repo->findAllByUser($allocation_id, $user_id);

        return response()->json($reservations, 200);
    }

    public function indexByUser($user_id)
    {
        $userRepo = new \App\User\UserRepository();
        $user = $userRepo->findById($user_id);
        $thisWeek = $this->repo->findByUserCurrentWeek($user_id);
        $fromToday = $this->repo->findByUserFromNextWeek($user_id);

        return response()->json([
            'user' => $user,
            'currentWeekReservations' => $thisWeek,
            'otherReservations' => $fromToday
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, AvailabilityRepository $availabilityRepository)
    {
        $newReservation = $request->input('reservation');

        $makeReservation = new MakeReservation($this->repo, $availabilityRepository, $newReservation);
        $message = $makeReservation();
        if ($message != 'created') {
            return response()->json($message, 400);
        }

        return response()->json('Reserva ingresada', 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, AvailabilityRepository $availabilityRepository)
    {
        $deleteReservation = new DeleteReservation($this->repo, $availabilityRepository, $id);
        $result = $deleteReservation();

        return response()->json($result, 200);

    }

    public function export($from, $to) 
    {
        return Excel::download(new ReservationsExport($from, $to), 'reservas_'. $from .'__' . $to . '.xlsx');
    }
}
