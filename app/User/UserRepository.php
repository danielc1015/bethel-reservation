<?php

namespace App\User;

use App\User\User;

class UserRepository
{
    public function findLikeId($id)
    {
        return User::where('id' , 'LIKE', '%' . $id . '%')->get()->load(['role']);
    }

    public function findById($user_id)
    {
        return User::findOrFail($user_id);
    }
    
    public function findByIdNotFail($user_id)
    {
        return User::find($user_id);
    }

    public function findByIdAndPassword($user_id, $password) {
        return User::where('id', $user_id)->where('password', $password)->where('role_id', 1)->firstOrFail();
    }
}
