<?php

namespace App\Exports;

use App\Reservation\Reservation;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ReservationsExport implements FromQuery, WithHeadings, ShouldAutoSize
{
    public function __construct(string $start_date, string $end_date) {
        $this->start_date = $start_date;
        $this->end_date = $end_date;
    }

    public function query()
        {
        // return Reservation::with('user:name');
            return Reservation::whereBetween('date', [$this->start_date, $this->end_date])
            ->leftJoin('allocations' , 'allocations.id' , '=' , 'reservations.allocation_id')
            ->leftJoin('users' , 'users.id' , '=' , 'reservations.user_id')
            ->select(['reservations.id', 'allocations.name as allocationna', 'reservations.date', 'reservations.start_time', 'reservations.end_time', 'users.name', 'reservations.created_at'])
            ->orderBy('users.name', 'asc')
            ->orderBy('reservations.date', 'asc');
        }


    public function headings(): array
    {
        return [
            '#',
            'Allocation',
            'Fecha',
            'Inicio',
            'Fin',
            'Usuario',
            'Creación',
        ];
    }
}
