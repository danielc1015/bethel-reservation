<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Allocation\AllocationRepository;
use App\Availability\AvailabilityDateAndTimeFormater;
use App\Availability\AvailabilityRepository;

class CreateAvailabilityRecords extends Command
{
    private $allocationRepository;
    private $availabilityRepository;
    private $start_date;
    private $end_time;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'availability:create {--from=x} {--to=x}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate availabilities records for a certain amount of days.
    By default, it generate availability records for 1 week starting today.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(AvailabilityRepository $availabilityRepository, AllocationRepository $allocationRepository)
    {
        parent::__construct();
        $this->allocationRepository = $allocationRepository;
        $this->availabilityRepository = $availabilityRepository;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Iniciando creación de registros de disponibilidad');
        \Log::info('Iniciando creación de registros de disponibilidad');
        
        // Get allocations
        $allocations = $this->allocationRepository->findAll();
        $this->setFromTo();
        $this->createAvailability($allocations);
        $this->info('Disponibilidad creada correctamente');
        \Log::info('Disponibilidad creada correctamente');
        return 0;
    }

    private function createAvailability($allocations) {
        foreach ($allocations as $allocation) {
            //crear disponiblidad
            $availabilityDateAndTimeFormater = new AvailabilityDateAndTimeFormater();
            $blocks = $availabilityDateAndTimeFormater($this->start_date, $this->end_date, $allocation->start_time, $allocation->end_time, $allocation->blockDuration);
            $availabilityRecords = $this->availabilityRepository->create($allocation->id, $allocation->maxPeopleSameTime, $blocks);
        }
    }

    private function setFromTo()
    {
        $this->start_date = $this->option('from');
        $this->end_date = $this->option('to');

        $from = now()->addDays(14)->format('Y-m-d'); // From the 2nd week from now
        $to   = now()->addDays(20)->format('Y-m-d'); // To the 3rd week from now

        /** If options where given */
        if ($this->option('from') == 'x' && $this->option('to') == 'x') {
            $this->start_date = $from;
            $this->end_date = $to;
        }

        $this->warn('Se generará disponibilidad desde ' . $this->start_date . '  hasta  ' . $this->end_date);
        \Log::info('Se generará disponibilidad desde ' . $this->start_date . '  hasta  ' . $this->end_date);
    }
}
