<?php

namespace App\Reservation;

interface IReservationRepository {
    public function findAll($allocation_id);
    public function findAllByUser($user_id);
    public function findById($reservation_id);
    public function create($reservation);
    public function edit($reservation_id, $reservation);
    public function delete($reservation_id);
}