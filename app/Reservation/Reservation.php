<?php

namespace App\Reservation;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $casts = [
        'blocks' => 'array'
    ];

    public function user()
    {
        return $this->belongsTo('App\User\User');
    }

    public function allocation()
    {
        return $this->belongsTo('App\Allocation\Allocation');
    }
}
