<?php

namespace App\Reservation;

use App\Reservation\IReservationRepository;
use Illuminate\Support\Carbon;

class AdminReservationRepository implements IReservationRepository
{
    public function findAll($allocation_id)
    {
        // Get first day of the week
        $today = now();
        $firstDayOfTheWeek = $today->startOfWeek(Carbon::MONDAY)->format('Y-m-d');

        // Get reservations starting from the current week
        $reservations = Reservation::where('allocation_id', $allocation_id)->get()->load(['user']);
        return $reservations;
    }

    public function findAllByUser($allocation_id, $user_id)
    {
        return null;
    }

    public function findById($reservation_id)
    {
        return null;
    }
    
    public function create($reservation)
    {
        return Reservation::create([
            'user_id'       => $reservation['user_id'],
            'allocation_id' => $reservation['allocation_id'],
            'date'          => $reservation['date'],
            'start_time'    => $reservation['start_time'],
            'end_time'      => $reservation['end_time'],
            'blocksNumber'  => count($reservation['blocks']),
            'blocks'        => $reservation['blocks'],
        ]);
    }

    public function countBlocksByUserAtAllocationAtDay($user_id, $allocation_id, $date)
    {
        return Reservation::where('user_id', $user_id)->where('allocation_id', $allocation_id)->where('date', $date)->sum('blocks');
    }

    public function edit($reservation_id, $reservation)
    {
        return null;
    }

    public function delete($reservation_id)
    {
        return null;
    }
}
