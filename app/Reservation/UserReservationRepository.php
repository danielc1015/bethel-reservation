<?php

namespace App\Reservation;

use App\Reservation\IReservationRepository;
use Illuminate\Support\Carbon;

class UserReservationRepository implements IReservationRepository
{
    private $start_time;
    private $end_time;
    public function findAll($allocation_id)
    {
        // Get first day of the week
        $today = now();
        $firstDayOfTheWeek = $today->startOfWeek(Carbon::MONDAY)->format('Y-m-d');

        // Get reservations starting from the current week
        $reservations = Reservation::where('date', '>=', $firstDayOfTheWeek)->where('allocation_id', $allocation_id)->get()->load(['allocation']);
        return $reservations;
    }
    
    public function findAllByUser($user_id)
    {
        // Get first day of the week
        $today = now();
        $todayFormated = $today->format('Y-m-d');

        // Get reservations starting from the current week
        $reservations = Reservation::where('date', '>=', $todayFormated)->where('user_id', $user_id)->orderBy('date', 'asc')->get()->load('allocation');
        return $reservations;
    }
    
    public function findByUserFromNextWeek($user_id)
    {
        // Get first day of the next week
        $nextWeek = Carbon::parse('next monday')->toDateString();

        // Get reservations starting from the current week
        $reservations = Reservation::where('date', '>=', $nextWeek)->where('user_id', $user_id)->orderBy('date', 'asc')->get()->load('allocation');
        return $reservations;
    }
   
    public function findByUserCurrentWeek($user_id)
    {
        // Get first day of the week
        $today = now();
        $firstDayOfTheWeek = Carbon::parse('today')->toDateString();
        $lastDayOfTheWeek = $today->endOfWeek(Carbon::SUNDAY)->format('Y-m-d');

        // Get reservations starting from the current week
        $reservations = Reservation::where('user_id', $user_id)->whereBetween('date', [$firstDayOfTheWeek, $lastDayOfTheWeek])->orderBy('date', 'asc')->get()->load('allocation');
        return $reservations;
    }

    public function findById($reservation_id)
    {
        return Reservation::findOrFail($reservation_id);
    }
    
    public function create($reservation)
    {
        return Reservation::create([
            'user_id'       => $reservation['user_id'],
            'allocation_id' => $reservation['allocation_id'],
            'date'          => $reservation['date'],
            'start_time'    => $reservation['start_time'],
            'end_time'      => $reservation['end_time'],
            'blocksNumber'  => count($reservation['blocks']),
            'blocks'        => $reservation['blocks'],
        ]);
    }

    public function countBlocksByUserAtAllocationAtDay($user_id, $allocation_id, $date)
    {
        return Reservation::where('user_id', $user_id)->where('allocation_id', $allocation_id)->where('date', $date)->sum('blocksNumber');
    }

    public function edit($reservation_id, $reservation)
    {
        return null;
    }

    public function delete($reservation_id)
    {
        return Reservation::find($reservation_id)->delete();
    }

    public function findReservationBetweenDates($user_id, $date, $start_time, $end_time)
    {
        $this->start_time = $start_time;
        $this->end_time = $end_time;

        $res = Reservation::where('date', $date)
        ->where('user_id', $user_id)
        ->whereRaw('? BETWEEN start_time AND end_time', [$this->start_time])
        ->first();
        if ($res != null) {
            return $res->load(['allocation']);
        }

        $res = Reservation::where('date', $date)
        ->where('user_id', $user_id)
        ->whereRaw('? BETWEEN start_time AND end_time', [$this->end_time])
        ->first();
        if ($res != null) {
            return $res->load(['allocation']);
        }

        $res = Reservation::where('date', $date)
        ->where('user_id', $user_id)
        ->whereRaw('? < start_time AND ? > end_time', [$this->start_time, $this->end_time])
        ->first();
        if ($res != null) {
            return $res->load(['allocation']);
        }
        
        $res = Reservation::where('date', $date)
        ->where('user_id', $user_id)
        ->whereRaw('start_time = ? AND ? = end_time', [$this->start_time, $this->end_time])
        ->first();
        if ($res != null) {
            return $res->load(['allocation']);
        }
        return $res;
    }
}
