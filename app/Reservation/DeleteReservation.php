<?php

namespace App\Reservation;

use Carbon\Carbon;

class DeleteReservation
{
    private $reservationRepository;
    private $availabilityRepository;
    private $reservationId;

    public function __construct($reservationRepository, $availabilityRepository, $reservationId) {
        $this->reservationRepository = $reservationRepository;
        $this->availabilityRepository = $availabilityRepository;
        $this->reservationId = $reservationId;
    }

    public function __invoke()
    {
        $reservation = $this->reservationRepository->findById($this->reservationId);
        
        if (!$this->canDelete($reservation)) {
            return ['error'];
        }
        \DB::transaction(function () use ($reservation) {
            $this->updateAvailability($reservation->blocks);
            $this->reservationRepository->delete($this->reservationId);
        });
        return ['success'];
    }

    private function updateAvailability($blocks)
    {
        foreach ($blocks as $block) {
            $this->availabilityRepository->editAvailability($block['id'], +1);
        }
    }

    /**
     * Check if the reservation is being deleted at least an hour before and the time of reservation has not started yet
     */
    public function canDelete($reservation)
    {
        $reservationTime = $reservation->date . ' ' . $reservation->start_time;
        $reservationDateTime = Carbon::createFromFormat('Y-m-d H:i:s', $reservationTime);
        $diff = now()->diffInHours($reservationDateTime);
        $hasPassed = now()->greaterThan($reservationDateTime);

        if ($diff < 1 || $hasPassed) {
            return false;
        }
        return true;
    }
}
