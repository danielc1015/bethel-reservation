<?php

namespace App\Reservation\Validation;

use App\Availability\AvailabilityRepository;

class AvailabilityValidator implements IValidable
{
    private $reservation;
    private $error;
    private $availabilityRepository;

    public function __construct($reservation) {
        $this->reservation = $reservation;
        $this->availabilityRepository = new AvailabilityRepository();
    }

    public function perform(): bool
    {
        $blocks = $this->reservation['blocks'];

        foreach ($blocks as $block) {
            $availability = $this->availabilityRepository->findById($block['id']);
            if ($availability->availability == 0) {
                $this->generateError($block);
                return false;
            }
        }
        return true;
    }
    
    public function getError(): string
    {
        return $this->error;
    }

    private function generateError($block)
    {
        $this->error = 'Sin disponibilidad entre ' . substr($block['start_time'], 0, 5) . ' y ' . substr($block['end_time'], 0, 5);
    }

}
