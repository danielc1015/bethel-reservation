<?php

namespace App\Reservation\Validation;

use App\Reservation\Validation\IValidable;
use App\Reservation\IReservationRepository;
use App\Availability\AvailabilityRepository;

class ReserveAtSameTimeValidator implements IValidable
{
    private $reservation;
    private $error;
    private $reservationRepository;

    public function __construct($reservation) {
        $this->reservation = $reservation;
        $this->reservationRepository = app(IReservationRepository::class)(auth()->user());
    }

    public function perform(): bool
    {
        $already = $this->reservationRepository->findReservationBetweenDates($this->reservation['user_id'] ,$this->reservation['date'], $this->reservation['start_time'], $this->reservation['end_time']);
        if ($already == null) {
            return true;
        }
        $this->error = 'Ya tienes una reserva en esa fecha entre ' . substr($already->start_time, 0, 5) . ' y ' . substr($already->end_time, 0, 5) . ' en ' . $already->allocation->name;
        return false;

    }

    public function getError(): string
    {
        return $this->error;
    }
}
