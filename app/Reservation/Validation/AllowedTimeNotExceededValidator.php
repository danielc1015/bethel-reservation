<?php

namespace App\Reservation\Validation;

use App\Reservation\IReservationRepository;
use App\Allocation\AllocationRepository;

class AllowedTimeNotExceededValidator implements IValidable
{
    private $reservation;
    private $error;
    private $reservationRepository;
    private $allocationRepository;

    public function __construct($reservation) {
        $this->reservation = $reservation;
        $this->reservationRepository = app(IReservationRepository::class)(auth()->user());
        $this->allocationRepository = new AllocationRepository();
    }

    public function perform(): bool
    {
        $allocation = $this->allocationRepository->findById($this->reservation['allocation_id']);
        $blocksNumber = count($this->reservation['blocks']);
        $reservationsMade = $this->reservationRepository->
                            countBlocksByUserAtAllocationAtDay($this->reservation['user_id'], $this->reservation['allocation_id'], $this->reservation['date']);

        if ( ($reservationsMade + $blocksNumber) > $allocation->maxBlocksAllowed ) {
            $this->generateError($allocation, $reservationsMade);
            return false;
        }
        return true;
    }

    public function getError(): string
    {
        return $this->error;
    }

    public function generateError($allocation, $reservationsMade)
    {
        $this->error = 'Puedes reservar hasta ' . $allocation->maxBlocksAllowed . ' bloques por día en '
                    . $allocation->name . '. Te quedan ' . ($allocation->maxBlocksAllowed - $reservationsMade);
    }
    
}
