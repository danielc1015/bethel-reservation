<?php

namespace App\Reservation\Validation;

interface IValidable
{
    public function perform(): bool;
    public function getError(): string;
}
