<?php

namespace App\Reservation\Validation;

use App\User\UserRepository;

class UserExistsValidator implements IValidable
{
    private $reservation;
    private $userRepository;

    public function __construct($reservation) {
        $this->reservation = $reservation;
        $this->userRepository = new UserRepository();
    }

    public function perform(): bool
    {
        $user_id = $this->reservation['user_id'];
        $user = $this->userRepository->findByIdNotFail($user_id);
        if ($user == null) {
            return false;
        }
        return true;
    }

    public function getError(): string
    {
        return 'El usuario ingresado no es válido';
    }
}
