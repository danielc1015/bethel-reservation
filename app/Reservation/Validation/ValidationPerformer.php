<?php

namespace App\Reservation\Validation;

class ValidationPerformer
{
    public $user_id;
    public $reservation;
    public $error;

    public $validators = [
        'App\Reservation\Validation\AvailabilityValidator',
        'App\Reservation\Validation\AllowedTimeNotExceededValidator',
        'App\Reservation\Validation\UserExistsValidator',
        'App\Reservation\Validation\ReserveAtSameTimeValidator',
    ];

    public function __construct($reservation)
    {
        $this->reservation = $reservation;
    }

    public function __invoke(): bool
    {
        foreach ($this->validators as $validator) {
            $validation = new $validator($this->reservation);

            if ($validation->perform() == false)
            {
                $this->error = $validation->getError();
                return false;
            }
        }
        return true;
    }


}
