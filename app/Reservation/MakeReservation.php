<?php

namespace App\Reservation;

use App\Reservation\Validation\ValidationPerformer;
use App\Reservation\ReservationRepository;
use App\Availability\AvailabilityRepository;

class MakeReservation
{
    private $reservationRepository;
    private $availabilityRepository;
    private $wasCreated = false;
    private $reservation;
    
    public function __construct($reservationRepository, $availabilityRepository, $reservation) {
        $this->reservationRepository = app(IReservationRepository::class)(auth()->user());
        $this->availabilityRepository = $availabilityRepository;
        $this->reservation = $reservation;
    }

    public function __invoke()
    {
        // Validate
        $validationMesagge = $this->validate();
        if ($validationMesagge != 'valid') {
            return $validationMesagge;
        }

        // Persist
        $reservation = $this->reservation;
        \DB::transaction(function () use ($reservation) {
            $this->reservationRepository->create($reservation);
            $this->updateAvailability($reservation['blocks']);
        });
        return 'created';
    }

    private function validate()
    {
        $performValidation = new ValidationPerformer($this->reservation);
        if ($performValidation()) {
            return 'valid';
        } else {
            return $performValidation->error;
        }
    }
    
    private function updateAvailability($blocks)
    {
        foreach ($blocks as $block){
            $this->availabilityRepository->editAvailability($block['id'], -1);
        }
    }

}
