<?php

namespace App\Availability;

use Illuminate\Support\Carbon;

class AvailabilityDateAndTimeFormater 
{

    public function __invoke($start_date, $end_date, $start_time, $end_time, $blockDuration)
    {

        $dateArray = $this->getDatesArray($start_date, $end_date);
        $timeArray = $this->getHoursArray($start_time, $end_time, $blockDuration);
        $blocks = array();

        foreach ($dateArray as $date) {
            foreach ($timeArray as $timeBlock) {

                $newBlock = [
                    'date' => $date,
                    'start_time' => $timeBlock['start_time'],
                    'end_time' => $timeBlock['end_time']
                ];
                $blocks[] = $newBlock;

            }
        }
        return $blocks;
    }

    /**
     * Fill array with dates between the given ones
     */
    private function getDatesArray($start_date, $end_date)
    {
        $firstDate = Carbon::createFromFormat('Y-m-d', $start_date);
        $lastDate = Carbon::createFromFormat('Y-m-d', $end_date);

        $date = $firstDate;
        $dates = array($date->toDateString());
        while ($date->lt($lastDate)) {
            $date->addDay();
            $dates[] = $date->toDateString();
        }
        return $dates;
    }

    /**
     * Fill array with hours between given times
     */
    private function getHoursArray($start_time, $end_time, $blockDuration)
    {
        $newBlock = [
            'start_time' => Carbon::createFromFormat('H:i:s', $start_time),
            'end_time' => Carbon::createFromFormat('H:i:s', $start_time)->addMinutes($blockDuration)
        ];
        // TODO: Line 33 could be deleted if the while condition become lte() and line 39 moves up to line 36
        $blocks = array($this->toTimeBlockFormat($newBlock));

        while ( $newBlock['end_time']->lt(Carbon::createFromFormat('H:i:s', $end_time)) ) {
            $newBlock['start_time']->addMinutes($blockDuration);
            $newBlock['end_time']->addMinutes($blockDuration);

            $blocks[] = $this->toTimeBlockFormat($newBlock);
        }
        return $blocks;
    }

    /**
     * 
     */
    private function toTimeBlockFormat($carbonBlock)
    {
        return [
            'start_time' => $carbonBlock['start_time']->toTimeString(),
            'end_time' => $carbonBlock['end_time']->toTimeString(),
        ];
    }


}
