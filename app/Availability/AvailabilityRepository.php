<?php

namespace App\Availability;

use App\Availability\Availability as Availability;

class AvailabilityRepository
{

    public function findById($availability_id)
    {
        return Availability::findOrFail($availability_id);
    }

    public function findAllFromDate($date)
    {
        return Availability::where('date', '>=', $date)->get()->load(['location']);
    }

    public function findByDateAndAllocation($date, $allocation_id)
    {
        return Availability::where('date', $date)->where('allocation_id', $allocation_id)->get();
    }

    public function create($allocation_id, $availability, $blocks)
    {
        foreach ($blocks as $block) {
            Availability::firstOrCreate([
                'allocation_id' => $allocation_id,
                'date' => $block['date'],
                'start_time' => $block['start_time'],
                'end_time' => $block['end_time']]
                ,
                ['allocation_id' => $allocation_id,
                'date' => $block['date'],
                'start_time' => $block['start_time'],
                'end_time' => $block['end_time'],
                'availability' => $availability
                ]);
        }
    }

    public function nothing()
    {
        print('hoal');
    }

    public function editAvailability($availability_id, $aditionAmmount)
    {
        \DB::table('availabilities')
            ->where('id', $availability_id)
            ->increment('availability', $aditionAmmount);
    }
}
