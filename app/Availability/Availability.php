<?php

namespace App\Availability;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Availability extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function allocation(Type $var = null)
    {
        return $this->belongsTo('App\Models\Allocation');
    }
}
