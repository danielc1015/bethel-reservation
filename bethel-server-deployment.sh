#!/bin/bash
repo="https://gitlab.com/danielc1015/bethel-reservation.git"
project-dir="./docker-compose-laravel/src"
NO_FORMAT="\033[0m"
F_UNDERLINED="\033[4m"
F_BOLD="\033[1m"
C_BLUE3="\033[38;5;20m"
C_DARKSEAGREEN="\033[48;5;108m"

echo -e "${F_BOLD}${C_BLUE3}---------------------------------"
echo -e "${F_BOLD}${C_BLUE3}|  N E W   D E P L O Y M E N T  |"
echo -e "${F_BOLD}${C_BLUE3}---------------------------------\n\n ${NO_FORMAT}"


echo -e "${F_BOLD}${F_UNDERLINED}${C_DARKSEAGREEN} >> Downloading new version from Gitlab ...${NO_FORMAT}\n"
git clone $repo ./new-version

echo -e "${F_BOLD}${F_UNDERLINED}${C_DARKSEAGREEN}\n\n >> Installing new version ...${NO_FORMAT}\n"
rm -rf ./docker-compose-laravel/src/app
mv ./new-version/app ./docker-compose-laravel/src/
echo " . "
rm -rf ./docker-compose-laravel/src/config
mv ./new-version/config ./docker-compose-laravel/src/
echo " . "
rm -rf ./docker-compose-laravel/src/database
mv ./new-version/database ./docker-compose-laravel/src/
echo " . "
rm -rf ./docker-compose-laravel/src/routes
mv ./new-version/routes ./docker-compose-laravel/src/
echo " . "
rm -rf ./new-version
echo " . "
echo -e "${F_BOLD}${F_UNDERLINED}${C_DARKSEAGREEN}\n\n >> NEW VERSION SUCCESSFULLY INSTALLED ${NO_FORMAT}\n"

exit